# Set up plugins for a Xen dom0 host
class munin::plugins::dom0 {

  munin::plugin::deploy {
    [ 'xen-multi' ]:
      config => 'user root';
  }
}
